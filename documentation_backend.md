# Documentation

## The application
Goal of this application is to label medical terms with MedDRA LLT and SOC codes. A kedro pipeline is used in the backend for preprocessing, different string matching algorithms and postprocessing. The pipelines as well as the input and output are documented in the following.

## Input and Output
The input and output file are .csv files. Accepted column names of the input file are:

| AE_ID | Original_AE_Term | Original_AE_Term_Spec |
|---|---|---|
|&nbsp; | | |

or:

| AE_ID | Original_Term |
|---|---|
|&nbsp; | |


The output file has the following columns:

| AE_ID | Original_AE_Term | Original_AE_Term_Spec | MedDRA_LLT_Code_1 | MedDRA_SOC_Code_1 | Probability_Correct_1 | MedDRA_LLT_Code_2 | MedDRA_SOC_Code_2 | Probability_Correct_2 |
|---|---|---|---|---|---|---|---|---|
|&nbsp; | | | | | | | | |



## Pipelines

In this project modular pipelines are used instead of one single main pipeline. They are logically separated and allow separation of concerns. Furthermore, pipelines could be instantiated multiple times and they are reusable. If requirements change or functionality has to be added, complete pipelines or single nodes can be added, removed or modified easily.

Using Kedro, a new pipeline can be created with: ```kedro pipeline create <pipeline_name>```. This command adds boilerplate folders and files for the designated pipeline to the project. A detailed documentation can be found [here](https://kedro.readthedocs.io/en/stable/nodes_and_pipelines/modular_pipelines.html).

The nodes of the each pipeline are implemented in the corresponding file nodes.py. The pipelines are defined in the pipeline.py files and their concatenation takes place in pipeline_registry.py. Configuration files for each pipeline are located in the folder conf\base\parameters. The input csv file is handled as a pandas.CSVDataSet and passed on between the nodes/ pipelines as pandas.DataFrame.

All functions of nodes working on a data frame always return the modified dataframe. Parameters are either specified in the configuration files or passed on between nodes/ pipelines.

&nbsp;
### Preprocessing

The preprocessing pipeline contains nodes to add and merge given columns. Furthermore, the content of columns can be lowercased and specified words can be removed.
<details>
<summary><strong>check_csv</strong>(csv: pd.DataFrame, requiredColumns: dict()) -> pd.DataFrame</summary>

Check if csv file contains all necessary columns set in the config.

Parameters:
- csv - The data set to work on.
- requiredColumns - List of columns that need to be present.

Notes:
Throws ColumnMissingException.
</details>

<details>
<summary><strong>lowercase_csv</strong>(csv: pd.DataFrame, columns: List[str]) -> pd.DataFrame</summary>

Lowercasing of content in the given columns.

Parameters:
- csv - The data set to work on.
- columns - List of columns for lowercasing.
</details>

<details>
<summary><strong>filter_columns</strong>(csv: pd.DataFrame, columns: List[str], filter: List[str]) -> pd.DataFrame</summary>

Remove words from a given list from the content of the columns.

Parameters:
- csv - The data set to work on.
- columns - List of columns for removing.
- filter - List of words to remove.
</details>

<details>
<summary><strong>merge_columns</strong>(csv: pd.DataFrame, mergedColumn: str, columns: List[str]) -> pd.DataFrame</summary>

Merge given columns.

Parameters:
- csv - The data set to work on.
- mergedColumn - Name of the resulting column.
- columns - List of columns for merging.

</details>

<details>
<summary><strong>add_columns</strong>(csv: pd.DataFrame, newColumns: List[str]) -> pd.DataFrame</summary>

Add given columns to data frame.

Parameters:
- csv - The data set to work on.
- newColumns - List of column names to add.
</details>

&nbsp;
### String matching

The stringmatching pipeline contains different algorithms for labeling the data.

<details>
<summary><strong>exact_match</strong>(csv: pd.DataFrame, columns: List[str], columnLabel: str, columnProbability: str, dictionary: dict) -> pd.DataFrame</summary>

Check if terms are equal to an a label (LLT term).

Parameters:
- csv - The data set to work on.
- columns - Columns on which the algorithm is applied.
- columnLabel - Name of the column containing the labels. (LLT for MedDRA)
- columnProbability - Name of the column containing the probability of the label being right.
- dictionary - Dictionary containing the terms (LLT terms) as key and the corresponding code (LLT code) as values.

Notes:
The function searches for exact matches and the output is at most one label. Due to this only one of the columns for labels is used.
</details>

<details>
<summary><strong>token_match</strong>(csv: pd.DataFrame, columns: List[str], columnLabel: str, columnProbability: str, probability: str, dictionary: dict) -> pd.DataFrame</summary>

Split terms into tokens (words) and check if a permutation of these tokens equals a given term (LLT).

Parameters:
- csv - The data set to work on.
- columns - Columns on which the algorithm is applied.
- columnLabel - Name of the column containing the labels. (LLT for MedDRA)
- columnProbability - Name of the column containing the probability of the label being right.
- probability - Probability for label beeing correct, can be set in config file.
- dictionary - Dictionary containing the terms (LLT terms) as key and the corresponding code (LLT code) as values.

Notes:
Duplicates are removed before comparison. Because of increasing complexity (and computation time) only entries consisting of 4 or less tokens are checked.
</details>

<details>
<summary><strong>spellcheck</strong>(csv: pd.DataFrame, columns: List[str], columnLabel: str, columnProbability: str, probability: str, dictionary: dict) -> pd.DataFrame</summary>

Split terms into tokens (words) and check if a permutation of these tokens equals a given term (LLT).

Parameters:
- csv - The data set to work on.
- columns - Columns on which the algorithm is applied.
- columnLabel - Name of the column containing the labels. (LLT for MedDRA)
- columnProbability - Name of the column containing the probability of the label being right.
- probability - Probability for label beeing correct, can be set in config file.
- dictionary - Dictionary containing the terms (LLT terms) as key and the corresponding code (LLT code) as values.

Notes:
The distance for spellchecking is set to 1. While the time needed increased significantly, only a few more entries were labeled.  
</details>

&nbsp;
### Experimental

The experimental pipeline contains nodes for labeling terms too. But comppared to the string matching pipeline, the algorithms have a much lower accuracy. While the number of right labels only slightly increases, significantly more wrong labels are output as well.

<details>
<summary><strong>slash_match</strong>(csv: pd.DataFrame, columnLabel: str, columnProbability: str, probability: str, dictionary: dict) -> pd.DataFrame</summary>

Split terms containing a slash in multiple parts. Each part is compared to the given terms. At most 2 matches are output.  

Parameters:
- csv - The data set to work on.
- columnLabel - Name of the column containing the first labels. (LLT for MedDRA)
- columnProbability - Name of the column containing the probability of the first label being right.
- columnLabel2 - Name of the column containing the second labels. (LLT for MedDRA)
- columnProbability2 - Name of the column containing the probability of the second label being right.
- probability - Probability for label beeing correct, can be set in config file.
- dictionary - Dictionary containing the terms (LLT terms) as key and the corresponding code (LLT code) as values.

</details>

<details>
<summary><strong>contains_llt</strong>(csv: pd.DataFrame, columnLabel: str, columnProbability: str, probability: str, dictionary: dict) -> pd.DataFrame</summary>

Check if term contains a given label (LLT). Found labels are sorted by distance. The two labels with the lowest distances are assigned.  

Parameters:
- csv - The data set to work on.
- columnLabel - Name of the column containing the first labels. (LLT for MedDRA)
- columnProbability - Name of the column containing the probability of the first label being right.
- columnLabel2 - Name of the column containing the second labels. (LLT for MedDRA)
- columnProbability2 - Name of the column containing the probability of the second label being right.
- probability - Probability for label beeing correct, can be set in config file.
- dictionary - Dictionary containing the terms (LLT terms) as key and the corresponding code (LLT code) as values.

Notes:
Especially this method outputs a high number of wrong terms. In clinical context this might be unwanted. Due to this the method is not part of the string matching pipeline.

</details>

&nbsp;
### Fuzzywuzzy

A pipeline containing nodes for an alternative approach to string matching using the fuzzywuzzy package.

<details>
<summary><strong>fuzzy_ratio_match</strong>(csv: pd.DataFrame, columns: List[str], columnLabel: str, columnProbability: str, dictionary: dict) -> pd.DataFrame</summary>

String matching using process.extractOne with ratio scorer.

Parameters:
- csv - The data set to work on.
- columns - Columns on which the algorithm is applied.
- columnLabel - Name of the column containing the labels. (LLT for MedDRA)
- columnProbability - Name of the column containing the probability of the label being right.
- dictionary - Dictionary containing the terms (LLT terms) as key and the corresponding code (LLT code) as values.

Notes:
Cutoff is set to 90 to increase the accuracy.
</details>

<details>
<summary><strong>fuzzy_token_sort_ratio</strong>(csv: pd.DataFrame, columns: List[str], columnLabel: str, columnProbability: str, dictionary: dict) -> pd.DataFrame</summary>

String matching using process.extractOne with token_sort_ratio scorer.

Parameters:
- csv - The data set to work on.
- columns - Columns on which the algorithm is applied.
- columnLabel - Name of the column containing the labels. (LLT for MedDRA)
- columnProbability - Name of the column containing the probability of the label being right.
- dictionary - Dictionary containing the terms (LLT terms) as key and the corresponding code (LLT code) as values.

Notes:
Cutoff is set to 90 to increase the accuracy.
</details>


&nbsp;
### Postprocessing

This pipeline contains nodes for adding SOCs and merging the original input data frame with the one created by the application to create the final output.

<details>
<summary><strong>add_socs</strong>(csv: pd.DataFrame, columnLlt1: str, columnSocs1: str, columnLlt2: str, columnSocs2: str, lltsToSocs: dict) -> pd.DataFrame</summary>

Add SOCs to entries with LLTs.

Parameters:
- csv - The data set to work on.
- columnLlt1 - Column containing the first LLT.
- columnSocs1 - Column for storing the SOCs of the first LLT.
- columnLlt2 - Column containing the second LLT.
- columnSocs2 - Column for storing the SOCs of the second LLT.
- lltsToSocs: Dictionary containing the LLT codes as key and a list of coresponding SOCs as value.

</details>

<details>
<summary><strong>merge_original_and_output</strong>(csvOriginal: pd.DataFrame, csvOutput: pd.DataFrame, columnsOutput: List[str], columnID: str) -> pd.DataFrame</summary>

Merge the input data frame and the data frame create in previous steps to obtain the final output.

Parameters:
- csvOriginal - The original data set.
- csvOutput - The data set created by the previous pipelines.
- columnsOutput - List of columns to keep in the final output.
- columnID - Column for merging.

</details>

&nbsp;
### Database

The pipeline contains nodes for accessing data from the local MedDRA .db file. Not all nodes listed below are used in the actual pipeline.

<details>
<summary><strong>get_llts</strong>(meddraDb: str) -> List[str]</summary>

Returns list of all LLTs from db.

Parameters:
- meddraDb - Path to the .db file.
</details>

<details>
<summary><strong>get_llts_with_codes</strong>(meddraDb: str) -> dict</summary>

Returns dictionary of all LLTs as keys and their codes as values.

Parameters:
- meddraDb - Path to the .db file.
</details>

<details>
<summary><strong>get_llts_and_pts</strong>(meddraDb: str) -> dict</summary>

Returns dictionary of all LLTs (codes) as keys and their PTs (codes) as values.

Parameters:
- meddraDb - Path to the .db file.
</details>

<details>
<summary><strong>get_pts_and_socs</strong>(meddraDb: str) -> dict</summary>

Returns dictionary of all PTs (codes) as keys and a list of the corresponding SOCs (codes) as values.

Parameters:
- meddraDb - Path to the .db file.
</details>

<details>
<summary><strong>get_llts_and_socs</strong>(meddraDb: str) -> dict</summary>

Returns dictionary of all LLTs (codes) as keys and a list of the corresponding SOCs (codes) as values.

Parameters:
- meddraDb - Path to the .db file.
</details>

<details>
<summary><strong>get_own_dict</strong>(csv: pd.DataFrame, columnKey: str, columnValues: str, dictToExpand: dict) -> dict</summary>

Reads own dictionary from csv file and adds it to a given one.

Parameters:
- csv - Dataframe for own dictionary.
- columnKey - Column of csv (own dict) which contains keys (LLT codes).
- columnValues - Column of csv (own dict) which contains keys (LLT terms).
- dictToExpand - Dictionary to which entries from own dict will be added.
</details>


&nbsp;
### Run the pipelines
To run pipelines they need to be registered in the ```pipeline_registry.py``` file. A default pipeline as well as additional ones can be defined. Using ```kedro run``` the default one will be executed. Specific pipelines can be run with ```kedro run --pipeline <pipeline_name>```. The following pipelines are defined:

| name | executed pipelines |
|---|---|
| default | database + preprocessing + stringmatching + postprocessing |
| experimental | database + preprocessing + stringmatching + experimental + postprocessing |
| fuzzy | database + preprocessing + fuzzy + postprocessing |


&nbsp;
## Additional modules

Additional to the functionality given by the pipelines and their nodes some files are inclueded in the project which do not need to be executed as pipeline. These files provide functions to initially create the database file and to change the configuration.

&nbsp;
### Create a database

To create a .db file using the MedDRA .asc files, ```create_database.py``` and ```meddra_database.py``` can be used. The function provided in the first file executes a given SQL script. For example, ```meddra_schema.sql``` contains a script to create all necessary tables and idices. The MedDRA data can then be added using the functions of the second file.

