"""
pipeline for database access
vontains nodes that return different dictionaries from database
"""

from kedro.pipeline import Pipeline, node, pipeline
from .nodes import *


def create_pipeline(**kwargs) -> Pipeline:
    return pipeline([
        node(
                func=get_llts_with_codes,
                inputs="params:db_path",
                outputs="llts",
                name="get_llts_node",
            ),
        # node(
        #         func=get_llts_and_pts,
        #         inputs="params:db_path",
        #         outputs="llts_pts",
        #         name="llts_and_pts_node",
        #     ),
        # node(
        #         func=get_pts_and_socs,
        #         inputs="params:db_path",
        #         outputs="pts_socs",
        #         name="get_pts_and_socs_node",
        #     ),
        node(
                func=get_llts_and_socs,
                inputs="params:db_path",
                outputs="llts_socs",
                name="get_llts_and_socs_node",
            ),
        node(
                func=get_own_dict,
                inputs=["dict_csv", "params:column_key", "params:column_values", "llts"],
                outputs="llts_with_own_dict",
                name="get_own_dict_node",
            ),
    ])
