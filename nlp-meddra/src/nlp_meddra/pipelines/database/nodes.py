"""
contains functions for the nodes in the database pipeline
"""


from typing import List, Tuple
import sqlite3
from sqlite3 import Error
import logging
import pandas as pd

def connect_to_db(db: str):
    connection = None

    try:
        connection = sqlite3.connect(db)
    except Error as e:
        logging.getLogger(__name__).warning(e)

    return connection


# returns list of llt names
def get_llts(meddraDb: str) -> List[str]:
    connection = connect_to_db(meddraDb)
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM llt")
    rows = cursor.fetchall()
    llts = [row[1].lower() if isinstance(row[1],str) else row[1] for row in rows]
    return llts


#returns dict with llt_name : llt_code
def get_llts_with_codes(meddraDb: str) -> dict:
    connection = connect_to_db(meddraDb)
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM llt")
    rows = cursor.fetchall()
    #llts = {row[0]: row[1].lower() if isinstance(row[1],str) else row[1] for row in rows}
    llts = {row[1].lower() if isinstance(row[1],str) else row[1]: row[0] for row in rows}
    return llts


#returns dict whith llt: pt
def get_llts_and_pts(meddraDb: str) -> dict:
    connection = connect_to_db(meddraDb)
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM llt")
    rows = cursor.fetchall()
    llts_pts = {row[0]: row[2] for row in rows}
    return llts_pts


#returns dict whith pt: [socs]
def get_pts_and_socs(meddraDb: str) -> dict:
    connection = connect_to_db(meddraDb)
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM mdhier")
    rows = cursor.fetchall()
    #llts_socs = {row[0]: row[3] for row in rows}
    pts_socs = {}
    for row in rows:
        if not row[0] in pts_socs.keys():
            pts_socs[row[0]] = [row[3]]
        else:
            pts_socs[row[0]] = pts_socs[row[0]] + [row[3]]
    return pts_socs


#returns dict with llt: [socs]
def get_llts_and_socs(meddraDb: str) -> dict:
    connection = connect_to_db(meddraDb)
    cursor = connection.cursor()
    cursor.execute("SELECT llt_code, soc_code FROM llt, mdhier WHERE llt.pt_code = mdhier.pt_code")
    rows = cursor.fetchall()
    llts_socs = {}
    for row in rows:
        if not row[0] in llts_socs.keys():
            llts_socs[row[0]] = [row[1]]
        else:
            llts_socs[row[0]] = llts_socs[row[0]] + [row[1]]
    return llts_socs


#add own dict entries
def get_own_dict(csv: pd.DataFrame, columnKey: str, columnValues: str, dictToExpand: dict) -> dict:
    csv = csv.applymap(lambda s:s.lower() if type(s) == str else s)
    own_dict = csv.set_index(columnKey)[columnValues].to_dict()
    return dictToExpand or own_dict