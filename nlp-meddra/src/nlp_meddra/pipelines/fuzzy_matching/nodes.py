"""
This is a boilerplate pipeline 'fuzzy_matching'
generated using Kedro 0.18.3
"""

from typing import List
import pandas as pd
from fuzzywuzzy import fuzz, process

def fuzzy_ratio_match(csv: pd.DataFrame, columns: List[str] = ["Original_Term"], columnLabel: str = "MedDRA_LLT_Code", columnProbability: str = "Probability_Correct", dictionary: dict = dict()) -> pd.DataFrame:
    dict = dictionary.keys()
    # for key in dictionary.keys():
    #     dict.extend(key.split(" "))
    
    for column in [c for c in columns if c in csv.columns]:
        n = 0
        for entry in csv[column]:
            match = process.extractOne(entry, dict, scorer=fuzz.ratio, score_cutoff=90)
            #print(entry + " -> " + str(match))
            if(match is not None):
                csv.loc[n, columnLabel] = dictionary[match[0]]
                csv.loc[n, columnProbability] = match[1]
            n += 1
    
    return csv

def fuzzy_token_sort_ratio(csv: pd.DataFrame, columns: List[str] = ["Original_Term"], columnLabel: str = "MedDRA_LLT_Code", columnProbability: str = "Probability_Correct", dictionary: dict = dict()) -> pd.DataFrame:
    dict = dictionary.keys()
    
    for column in [c for c in columns if c in csv.columns]:
        n = 0
        for entry in csv[column]:
            if str(csv.at[n, columnLabel]) == "":
                match = process.extractOne(entry, dict, scorer=fuzz.token_sort_ratio, score_cutoff=90)
                #print(entry + " -> " + str(match))
                if(match is not None):
                    csv.loc[n, columnLabel] = dictionary[match[0]]
                    csv.loc[n, columnProbability] = match[1]
            n += 1
    
    return csv