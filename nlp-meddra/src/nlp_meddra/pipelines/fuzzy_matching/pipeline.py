"""
This is a boilerplate pipeline 'fuzzy_matching'
generated using Kedro 0.18.3
"""

from kedro.pipeline import Pipeline, node, pipeline
from .nodes import *


def create_pipeline(**kwargs) -> Pipeline:
    return pipeline([
        node(
                func=fuzzy_ratio_match,
                inputs=["csv_input_stringmatching", "params:columns_for_stringmatching", "params:column_llt_stringmatching_1", "params:column_probability_stringmatching_1", "llts_with_own_dict"],
                outputs="csv_fuzzy_ratio_match",
                name="fuzzy_stringmatching_node",
            ),
        node(
                func=fuzzy_token_sort_ratio,
                inputs=["csv_fuzzy_ratio_match", "params:columns_for_stringmatching", "params:column_llt_stringmatching_1", "params:column_probability_stringmatching_1", "llts_with_own_dict"],
                outputs="csv_fuzzy_match",
                name="fuzzy_token_set_ratio_node",
            ),
    ])
