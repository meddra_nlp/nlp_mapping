"""
contains functions for the nodes in the string matching pipeline

"""

from typing import List
import pandas as pd
import re
from spellchecker import SpellChecker

def exact_match(csv: pd.DataFrame, columns: List[str] = ["Original_AE_Term_Spec","Original_Term"], columnLabel: str = "MedDRA_LLT_Code", columnProbability: str = "Probability_Correct", dictionary: dict = dict()) -> pd.DataFrame:
    for column in [c for c in columns if c in csv.columns]:
        csv[column] = csv[column].astype(str).replace("n.a.", "")
        n = 0
        for entry in csv[column]:
            if entry in dictionary.keys() and str(csv.at[n, columnLabel]) == "":
                csv.loc[n, columnLabel] = dictionary[entry]
                csv.loc[n, columnProbability] = 100
            n+=1
        print("#codes: " + str(sum(1 for e in csv[columnLabel] if len(str(e)) != 0)))
    return csv


def token_match(csv: pd.DataFrame, columns: List[str] = ["Original_Term"], columnLabel: str = "MedDRA_LLT_Code", columnProbability: str = "Probability_Correct", probability: str = "80", dictionary: dict = dict()) -> pd.DataFrame:
    labels_as_tokens = [re.split(" |-", s) for s in dictionary.keys()]
    for column in [c for c in columns if c in csv.columns]:
        n = 0
        for entry in csv[column]:
            tokens = re.split(" |-", entry)
            tokens = list(set(tokens)) #remove duplicates
            if str(csv.at[n, columnLabel]) == "" and len(tokens) <= 4:
                for permutation in [l for l in perm(tokens) if l in labels_as_tokens]:
                    index = labels_as_tokens.index(permutation)
                    key = [k for k in dictionary][index]
                    csv.loc[n, columnLabel] = dictionary[key]
                    csv.loc[n, columnProbability] = probability
            n+=1
        print("#codes: " + str(sum(1 for e in csv[columnLabel] if len(str(e)) != 0)))
    return csv


def spellcheck(csv: pd.DataFrame, columns: List[str] = ["Original_Term"], columnLabel: str = "MedDRA_LLT_Code", columnProbability: str = "Probability_Correct", probability: str = "5", dictionary: dict = dict()) -> pd.DataFrame:
    spell = SpellChecker(language=None)
    dictSpellcheck = []
    for key in dictionary.keys():
        dictSpellcheck.extend(key.split(" "))

    spell.word_frequency.load_words(dictSpellcheck)
    #spell.distance = 2
    spell.distance = 1

    for column in [c for c in columns if c in csv.columns]:
        n=0
        for entry in csv[column]:
            if str(csv.at[n, columnLabel]) == "":
                words = [w for w in entry.split(" ")]
                entryCorrected = ""
                for word in words:
                    correction = spell.correction(word)
                    if correction != None:
                        entryCorrected += (correction + " ")
                    else:
                        entryCorrected += (word + " ")
                if entryCorrected.strip() in dictionary.keys():
                    #print(entry + " -> " + entryCorrected + "  " + str(csv.loc[n, "AE_ID"]))
                    key = dictionary[entryCorrected.strip()]
                    csv.loc[n, columnLabel] = key
                    csv.loc[n, columnProbability] = probability
            n += 1
        print("#codes: " + str(sum(1 for e in csv[columnLabel] if len(str(e)) != 0)))

    return csv

# helper functions
def addperm(x,l):
    return [ l[0:i] + [x] + l[i:]  for i in range(len(l)+1) ]

def perm(l):
    if len(l) == 0:
        return [[]]
    return [x for y in perm(l[1:]) for x in addperm(l[0],y) ]