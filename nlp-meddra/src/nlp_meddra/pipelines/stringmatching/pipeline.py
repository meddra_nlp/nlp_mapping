"""
pipeline for string matching
contains nodes for exact and token matching as well as spellchecking
"""

from kedro.pipeline import Pipeline, node, pipeline
from .nodes import *


def create_pipeline(**kwargs) -> Pipeline:
    return pipeline([
        node(
                func=exact_match,
                inputs=["csv_input_stringmatching", "params:columns_for_stringmatching", "params:column_llt_stringmatching_1", "params:column_probability_stringmatching_1", "llts_with_own_dict"],
                outputs="csv_exact_match",
                name="exact_stringmatching_node",
            ),
        node(
                func=token_match,
                inputs=["csv_exact_match", "params:columns_for_stringmatching", "params:column_llt_stringmatching_1", "params:column_probability_stringmatching_1", "params:probability_correct_tokens", "llts_with_own_dict"],
                outputs="csv_token_match",
                name="token_stringmatching_node",
            ),
        node(
                func=spellcheck,
                inputs=["csv_token_match", "params:columns_for_stringmatching", "params:column_llt_stringmatching_1", "params:column_probability_stringmatching_1", "params:probability_correct_spellcheck", "llts_with_own_dict"],
                outputs="csv_spellcheck",
                name="spellcheck_node",
            ),
    ])
