"""
contains functions for nodes in the experimental pipeline
"""

from typing import List
import pandas as pd
from Levenshtein import distance
import re
#from fuzzywuzzy import fuzz #, StringMatcher
#from fuzzywuzzy import process

def slash_match(csv: pd.DataFrame, columns: List[str] = ["Original_Term"], columnLabel: str = "MedDRA_LLT_Code", columnProbability: str = "Probability_Correct", columnLabel2: str = "MedDRA_LLT_Code_2", columnProbability2: str = "Probability_Correct_2", probability: str = "15", dictionary: dict = dict()) -> pd.DataFrame:
    for column in [c for c in columns if c in csv.columns]:
        n = 0 
        for entry in csv[column]:
            if ("/" in entry) and str(csv.at[n, columnLabel]) == "":
                keys = []
                tokens = entry.split("/")
                for token in [t for t in tokens if t in dictionary.keys()]:
                    key = dictionary[token]
                    keys.append(key)
                if len(keys) == 1:
                    csv.loc[n, columnLabel] = key
                    csv.loc[n, columnProbability] = probability
                elif len(keys) >= 2:
                    csv.loc[n, columnLabel] = keys[0]
                    csv.loc[n, columnProbability] = probability
                    csv.loc[n, columnLabel2] = keys[1]
                    csv.loc[n, columnProbability2] = probability    
            n+=1
        print("#codes: " + str(sum(1 for e in csv[columnLabel] if len(str(e)) != 0)))   
    return csv


def contains_llt(csv: pd.DataFrame, columns: List[str] = ["Original_Term"], columnLabel: str = "MedDRA_LLT_Code", columnProbability: str = "Probability_Correct", columnLabel2: str = "MedDRA_LLT_Code_2", columnProbability2: str = "Probability_Correct_2", probability: str = "1", dictionary: dict = dict()) -> pd.DataFrame:
    for column in [c for c in columns if c in csv.columns]:
        n = 0
        for entry in csv[column]:
            if "(" in entry:
                print(entry)
            if str(csv.at[n, columnLabel]) == "":
                entry2 =re.sub("\(.*?\)","", entry)  # remove everything between ( )
                terms_and_dist = {}
                for term in [t for t in dictionary.keys() if len(t) > 3]:
                    if term in entry2:
                        #print("entry: " + entry + "   llt: " + term)
                        #terms.append(term)
                        terms_and_dist[term] = distance(term, entry)

                if len(terms_and_dist) > 0:
                    terms_and_dist = dict(sorted(terms_and_dist.items(), key=lambda item: item[1])) 
                    #print(terms_and_dist.keys)
                    key = dictionary[next(iter(terms_and_dist))]
                    csv.loc[n, columnLabel] = key
                    csv.loc[n, columnProbability] = probability

                    if len(terms_and_dist) >= 2:
                        key = dictionary[next(iter(terms_and_dist))]
                        csv.loc[n, columnLabel2] = key
                        csv.loc[n, columnProbability2] = probability
            n += 1
    return csv

def fuzzywuzzy(csv: pd.DataFrame, columns: List[str] = ["Original_Term"], columnLabel: str = "MedDRA_LLT_Code", columnProbability: str = "Probability_Correct", defaultProbability: str = "62.5", dictionary: dict = dict()) -> pd.DataFrame:
    for column in [c for c in columns if c in csv.columns]:
        n = 0
        for entry in csv[column]:
            if str(csv.at[n, columnLabel]) == "" and len(entry)<= 20:
                llts = dictionary.values()
                #list = process.extractOne(entry, llts, scorer=fuzz.ratio)
                #print(entry + "  " + str(list))
                #for value in strings.values():
                    #if(fuzz.partial_token_sort_ratio(entry, value)) >= 60:
                    #    print(entry + "   " + value + "   " + str(fuzz.token_sort_ratio(entry, value)))
            n += 1
    return csv