"""
pipeline for experimental string matching methods
contains nodes for slash match, check if llt is contained in term
"""

from kedro.pipeline import Pipeline, node, pipeline
from .nodes import *


def create_pipeline(**kwargs) -> Pipeline:
    return pipeline([
        node(
                func=slash_match,
                inputs=["csv_input_experimental", "params:columns_for_experimental_stringmatching", "params:column_probability_experimental_1", "params:column_llt_experimental_2", "params:column_probability_experimental_2", "params:probability_correct_experimental", "llts_with_own_dict"],
                outputs="csv_slash_match",
                name="slash_stringmatching_node",
            ),
        node(
                func=contains_llt,
                inputs=["csv_slash_match", "params:columns_for_experimental_stringmatching", "params:column_llt_experimental_1", "params:column_probability_experimental_1", "params:column_llt_experimental_2", "params:column_probability_experimental_2", "params:probability_correct_experimental", "llts_with_own_dict"],
                outputs="csv_contains_llt",
                name="contains_llt_node",
            ),
        node(
                func=fuzzywuzzy,
                inputs=["csv_contains_llt", "params:columns_for_experimental_stringmatching", "params:column_llt_experimental_1", "params:column_probability_experimental_1", "params:probability_correct_experimental", "llts_with_own_dict"],
                outputs="csv_fuzzywuzzy",
                name="fuzzywuzzy_node",
            ),
    ])
