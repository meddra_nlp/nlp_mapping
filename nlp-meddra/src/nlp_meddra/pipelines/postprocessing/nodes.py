"""
contains functions for the nodes in the postprocessing pipeline

"""

from typing import List
import pandas as pd
import math

def llt_to_pt(csv: pd.DataFrame, column: str = "MedDRA_LLT_Code", codes: dict = dict()) -> pd.DataFrame:
    n = 0
    pts = codes.values()
    for entry in csv[column]:        
        if entry != "" and entry != "NaN": #not math.isnan(entry): #
            entry = int(entry)
            if not entry in pts:  
                #print(str(entry) + "  " + str(codes[entry]))
                csv.loc[n, column] = codes[entry]
        n += 1
    return csv


def add_socs(csv: pd.DataFrame, columnLlt1: str = "MedDRA_LLT_Code", columnSocs1: str = "MedDRA_SOC_Code", columnProbalbility1: str = "", columnLlt2: str = "MedDRA_LLT_Code", columnSocs2: str = "MedDRA_SOC_Code", columnProbalbility2: str = "", lltsToSocs: dict = dict()) -> pd.DataFrame:
    n = 0
    for entry in csv[columnLlt1]:
        if entry != "" and entry != "NaN": #if not math.isnan(entry):
            labels = lltsToSocs[entry]
            csv.loc[n, columnSocs1] = ', '.join(str(l) for l in labels)
            csv.loc[n, columnProbalbility1] = int(float(csv.loc[n, columnProbalbility1]) / len(labels))

        n += 1

    n = 0

    for entry in csv[columnLlt2]:
        if entry != "" and entry != "NaN": #if not math.isnan(entry):
            labels = lltsToSocs[entry]
            csv.loc[n, columnSocs2] = ', '.join(str(l) for l in labels)
            csv.loc[n, columnProbalbility2] = int(float(csv.loc[n, columnProbalbility2]) / len(labels))

        n += 1

    return csv


def merge_original_and_output(csvOriginal: pd.DataFrame, csvOutput: pd.DataFrame, columnsOutput: List[str], columnID: str) -> pd.DataFrame:
    for column in [c for c in csvOutput.columns if c not in columnsOutput]:
        csvOutput = csvOutput.drop(column, axis=1)
    csv = pd.merge(csvOriginal, csvOutput, how="inner", on=columnID)
    return csv