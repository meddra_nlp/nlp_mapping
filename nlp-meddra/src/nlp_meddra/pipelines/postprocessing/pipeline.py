"""
pipeline for postprocessing
contains nodes for adding SOCs and merging data frames
"""

from kedro.pipeline import Pipeline, node, pipeline
from .nodes import *


def create_pipeline(**kwargs) -> Pipeline:
    return pipeline([
        # node(
        #         func=llt_to_pt,
        #         inputs=["post_csv", "params:column_leaf", "pt_dict"],
        #         outputs="csv_pts",
        #         name="pts_node",
        #     ),
        node(
                func=add_socs,
                inputs=["csv_input_postprocessing", "params:column_llt_1", "params:column_socs_1", "params:column_probability_1", "params:column_llt_2", "params:column_socs_2", "params:column_probability_2", "llts_socs"],
                outputs="csv_with_socs",
                name="socs_node",
            ),
        node(
                func=merge_original_and_output,
                inputs=["input_data","csv_with_socs", "params:columns_to_keep_merge", "params:column_for_merge"],
                outputs="csv_final",
                name="merging_postprocessing_node",
        )
    ])
