"""
pipeline for preprocessing
contains nodes for lowercasing, filtering, merging and adding columns
"""

from kedro.pipeline import Pipeline, node, pipeline
from .nodes import *


def create_pipeline(**kwargs) -> Pipeline:
    return pipeline([
        node(
            func=check_csv,
            inputs=["csv_input_preprocessing", "params:required_columns"],
            outputs="csv_checked",
            name="check_csv_node",
        ),
        node(
            func=lowercase_columns,
            inputs=["csv_input_preprocessing", "params:columns_for_lowercasing"],
            outputs="csv_lowercase",
            name="lowercase_columns_node",
        ),
        node(
            func=filter_columns,
            inputs=["csv_lowercase","params:columns_for_filtering","params:filter"],
            outputs="csv_filtered",
            name="filter_columns_node",
        ),
        node(
            func=merge_columns,
            inputs="csv_filtered",
            outputs="csv_merged",
            name="merge_columns_node",
        ),
        node(
            func=add_columns,
            inputs=["csv_merged","params:columns_to_add"],
            outputs="csv_add",
            name="add_columns_node",
        ),
    ])
