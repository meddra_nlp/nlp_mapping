"""
contains functions for the nodes in the preprocessing pipeline

"""

from typing import List
import pandas as pd

def check_csv(csv: pd.DataFrame, requiredColumns: dict()):
    for key, listOfColumns in requiredColumns.items():
        if all(column in list(csv.columns) for column in listOfColumns):
            return csv
    raise ColumnMissingException

def lowercase_columns(csv: pd.DataFrame, columns: List[str] = ["Original_AE_Term", "Original_AE_Term_Spec"]) -> pd.DataFrame:
    for column in [c for c in columns if c in csv.columns]:
        csv[column] = csv[column].map(lambda x: x.lower() if isinstance(x,str) else x)
    return csv

def filter_columns(csv: pd.DataFrame, columns: List[str] = ["Original_AE_Term"], filter: List[str] = [""]) -> pd.DataFrame:
    for column in [c for c in columns if c in csv.columns]:
        csv[column] = csv[column].apply(lambda x: " ".join([word for word in str(x).split() if word not in filter]))
    return csv

def merge_columns(csv: pd.DataFrame, mergedColumn: str = "Original_Term", columns: List[str] = ["Original_AE_Term", "Original_AE_Term_Spec"]) -> pd.DataFrame:
    col = []
    for column in [c for c in columns if c in csv.columns]:
        col.append(column)
    if len(col) >= 2:
        csv[mergedColumn] = csv[col].astype(str).apply(" ".join, axis=1)
    return csv

def add_columns(csv: pd.DataFrame, newColumns: List[str] = ["MedDRA_LLT_Code", "MedDRA_SOC_Code", "Probability_Correct"]) -> pd.DataFrame:
    for c in newColumns:
        if c not in csv.columns:
            csv[c] = ""
    return csv


class ColumnMissingException(Exception):
    "Raised when csv does not contain all required columns."
    pass