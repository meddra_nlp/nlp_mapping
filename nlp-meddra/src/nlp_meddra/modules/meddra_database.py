import sqlite3
from sqlite3 import Error

path_ascii = "../data/01_raw/MedAscii"

def connect_to_db(db_file):
    connection = None

    try:
        connection = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return connection

#llt
def create_llt(connection, llt):
    sql = ''' INSERT INTO llt(llt_code,llt_name,pt_code,llt_whoart_code,llt_harts_code,llt_costart_sym,llt_icd9_code,llt_icd9cm_code,
        llt_icd10_code,llt_currency,llt_jart_code) VALUES (?,?,?,?,?,?,?,?,?,?,?) '''
    
    cursor = connection.cursor()
    cursor.execute(sql,llt)
    connection.commit()

def table_llt(connection):
    file_path = path_ascii + "/llt.asc"

    with connection:
        f = open(file_path, 'r', encoding="latin-1")
        lines = f.readlines()

        for line in lines:
            line = line[:-2]
            llt = line.split('$')
            create_llt(connection, (llt[0], llt[1], llt[2], llt[3], llt[4], llt[5], llt[6], llt[7], llt[8], llt[9], llt[10]))

        f.close()


#pt
def create_pt(connection, pt):
    sql = ''' INSERT INTO pt(pt_code,pt_name,null_field,pt_soc_code,pt_whoart_code,pt_harts_code,pt_costart_sym,pt_icd9_code,pt_icd9cm_code,
        pt_icd10_code,pt_jart_code) VALUES (?,?,?,?,?,?,?,?,?,?,?) '''
    
    cursor = connection.cursor()
    cursor.execute(sql,pt)
    connection.commit()


def table_pt(connection):
    file_path = path_ascii + "/pt.asc"

    with connection:
        f = open(file_path, 'r', encoding="latin-1")
        lines = f.readlines()

        for line in lines:
            line = line[:-2]
            pt = line.split('$')
            create_pt(connection, (pt[0], pt[1], pt[2], pt[3], pt[4], pt[5], pt[6], pt[7], pt[8], pt[9], pt[10]))

        f.close()


#hlt
def create_hlt(connection, hlt):
    sql = ''' INSERT INTO hlt(hlt_code,hlt_name,hlt_whoart_code,hlt_harts_code,hlt_costart_sym,hlt_icd9_code,hlt_icd9cm_code,
        hlt_icd10_code,hlt_jart_code) VALUES (?,?,?,?,?,?,?,?,?) '''
    
    cursor = connection.cursor()
    cursor.execute(sql,hlt)
    connection.commit()


def table_hlt(connection):
    file_path = path_ascii + "/hlt.asc"

    with connection:
        f = open(file_path, 'r', encoding="latin-1")
        lines = f.readlines()

        for line in lines:
            line = line[:-2]
            hlt = line.split('$')
            create_hlt(connection, (hlt[0], hlt[1], hlt[2], hlt[3], hlt[4], hlt[5], hlt[6], hlt[7], hlt[8]))

        f.close()


#hlt_pt
def create_hlt_pt(connection, hlt_pt):
    sql = ''' INSERT INTO hlt_pt(hlt_code,pt_code) VALUES (?,?) '''
    
    cursor = connection.cursor()
    cursor.execute(sql,hlt_pt)
    connection.commit()


def table_hlt_pt(connection):
    file_path = path_ascii + "/hlt_pt.asc"

    with connection:
        f = open(file_path, 'r', encoding="latin-1")
        lines = f.readlines()

        for line in lines:
            line = line[:-2]
            hlt_pt = line.split('$')
            create_hlt_pt(connection, (hlt_pt[0], hlt_pt[1]))

        f.close()


#hlgt
def create_hlgt(connection, hlgt):
    sql = ''' INSERT INTO hlgt(hlgt_code,hlgt_name,hlgt_whoart_code,hlgt_harts_code,hlgt_costart_sym,hlgt_icd9_code,hlgt_icd9cm_code,
        hlgt_icd10_code,hlgt_jart_code) VALUES (?,?,?,?,?,?,?,?,?) '''
    
    cursor = connection.cursor()
    cursor.execute(sql,hlgt)
    connection.commit()


def table_hlgt(connection):
    file_path = path_ascii + "/hlgt.asc"

    with connection:
        f = open(file_path, 'r', encoding="latin-1")
        lines = f.readlines()

        for line in lines:
            line = line[:-2]
            hlgt = line.split('$')
            create_hlgt(connection, (hlgt[0], hlgt[1], hlgt[2], hlgt[3], hlgt[4], hlgt[5], hlgt[6], hlgt[7], hlgt[8]))

        f.close()


#hlgt_hlt
def create_hlgt_hlt(connection, hlgt_hlt):
    sql = ''' INSERT INTO hlgt_hlt(hlgt_code,hlt_code) VALUES (?,?) '''
    
    cursor = connection.cursor()
    cursor.execute(sql,hlgt_hlt)
    connection.commit()


def table_hlgt_hlt(connection):
    file_path = path_ascii + "/hlgt_hlt.asc"

    with connection:
        f = open(file_path, 'r', encoding="latin-1")
        lines = f.readlines()

        for line in lines:
            line = line[:-2]
            hlgt_hlt = line.split('$')
            create_hlgt_hlt(connection, (hlgt_hlt[0], hlgt_hlt[1]))

        f.close()


#soc
def create_soc(connection, soc):
    sql = ''' INSERT INTO soc(soc_code,soc_name,soc_abbrev,soc_whoart_code,soc_harts_code,soc_costart_sym,soc_icd9_code,soc_icd9cm_code,
        soc_icd10_code,soc_jart_code) VALUES (?,?,?,?,?,?,?,?,?,?) '''
    
    cursor = connection.cursor()
    cursor.execute(sql,soc)
    connection.commit()


def table_soc(connection):
    file_path = path_ascii + "/soc.asc"

    with connection:
        f = open(file_path, 'r', encoding="latin-1")
        lines = f.readlines()

        for line in lines:
            line = line[:-2]
            soc = line.split('$')
            create_soc(connection, (soc[0], soc[1], soc[2], soc[3], soc[4], soc[5], soc[6], soc[7], soc[8], soc[9]))

        f.close()


#soc_hlgt
def create_soc_hlgt(connection, soc_hlgt):
    sql = ''' INSERT INTO soc_hlgt(soc_code,hlgt_code) VALUES (?,?) '''
    
    cursor = connection.cursor()
    cursor.execute(sql,soc_hlgt)
    connection.commit()


def table_soc_hlgt(connection):
    file_path = path_ascii + "/soc_hlgt.asc"

    with connection:
        f = open(file_path, 'r', encoding="latin-1")
        lines = f.readlines()

        for line in lines:
            line = line[:-2]
            soc_hlgt = line.split('$')
            create_soc_hlgt(connection, (soc_hlgt[0], soc_hlgt[1]))

        f.close()


#mdhier
def create_mdhier(connection, mdhier):
    sql = ''' INSERT INTO mdhier(pt_code,hlt_code,hlgt_code,soc_code,pt_name,hlt_name,hlgt_name,soc_name,soc_abbrev,null_field,pt_soc_code,
        primary_soc_fg) VALUES (?,?,?,?,?,?,?,?,?,?,?,?) '''
    
    cursor = connection.cursor()
    cursor.execute(sql,mdhier)
    connection.commit()


def table_mdhier(connection):
    file_path = path_ascii + "/mdhier.asc"

    with connection:
        f = open(file_path, 'r', encoding="latin-1")
        lines = f.readlines()

        for line in lines:
            line = line[:-2]
            mdhier = line.split('$')
            create_mdhier(connection, (mdhier[0], mdhier[1], mdhier[2], mdhier[3], mdhier[4], mdhier[5], mdhier[6], mdhier[7], mdhier[8], mdhier[9], mdhier[10], mdhier[11]))

        f.close()


#intl_ord
def create_intl_ord(connection, intl_ord):
    sql = ''' INSERT INTO intl_ord(intl_ord_code,soc_code) VALUES (?,?) '''
    
    cursor = connection.cursor()
    cursor.execute(sql,intl_ord)
    connection.commit()


def table_intl_ord(connection):
    file_path = path_ascii + "/intl_ord.asc"

    with connection:
        f = open(file_path, 'r', encoding="latin-1")
        lines = f.readlines()

        for line in lines:
            line = line[:-2]
            intl_ord = line.split('$')
            create_intl_ord(connection, (intl_ord[0], intl_ord[1]))

        f.close()


#smq_list
def create_smq_list(connection, smq_list):
    sql = ''' INSERT INTO smq_list(smq_code,smq_name,smq_level,smq_description,smq_source,smq_note,MedDRA_version,status,smq_algorithm) VALUES (?,?,?,?,?,?,?,?,?) '''
    
    cursor = connection.cursor()
    cursor.execute(sql,smq_list)
    connection.commit()


def table_smq_list(connection):
    file_path = path_ascii + "/smq_list.asc"

    with connection:
        f = open(file_path, 'r', encoding="latin-1")
        lines = f.readlines()

        for line in lines:
            line = line[:-2]
            smq_list = line.split('$')
            create_smq_list(connection, (smq_list[0], smq_list[1], smq_list[2], smq_list[3], smq_list[4], smq_list[5], smq_list[6], smq_list[7], smq_list[8]))

        f.close()


#smq_content
def create_smq_content(connection, smq_content):
    sql = ''' INSERT INTO smq_content(smq_code,term_code,term_level,term_scope,term_category,term_weight,term_status,term_addition_version,
            term_last_modified_version) VALUES (?,?,?,?,?,?,?,?,?) '''
    
    cursor = connection.cursor()
    cursor.execute(sql,smq_content)
    connection.commit()


def table_smq_content(connection):
    file_path = path_ascii + "/smq_content.asc"

    with connection:
        f = open(file_path, 'r', encoding="latin-1")
        lines = f.readlines()

        for line in lines:
            line = line[:-2]
            smq_content = line.split('$')
            create_smq_content(connection, (smq_content[0], smq_content[1], smq_content[2], smq_content[3], smq_content[4], smq_content[5], smq_content[6], smq_content[7], smq_content[8]))

        f.close()


def fill_all(database_file):
    connection = connect_to_db(database_file)
    table_llt(connection)
    table_pt(connection)
    table_hlt(connection)
    table_hlt_pt(connection)
    table_hlgt(connection)
    table_hlgt_hlt(connection)
    table_soc(connection)
    table_soc_hlgt(connection)
    table_mdhier(connection)
    table_intl_ord(connection)
    table_smq_list(connection)
    table_smq_content(connection)

    connection.close()