import sqlite3
from sqlite3 import Error

# execute a SQL script on a given .db file

def create_database(sql_file, database_file):
    
    connection = None

    try:
        with open(sql_file, 'r') as sql_file:
            sql = sql_file.read()
        connection = sqlite3.connect(database_file)
        cs = connection.cursor()
        cs.executescript(sql)
        connection.commit()
    except Error as e:
        print(e)
    finally:
        if connection:
            connection.close()
            return True
        else:
            return False