import yaml

# configuration files can be found in: nlp-meddra\conf\base\parameters\<filename>.yml

def read_config(path: str):
    try:
        with open(path) as file:
            config = yaml.load(file, Loader=yaml.FullLoader)
            return config
    except Exception as e:
        return "{'exception': {}}".format(e)
    

def change_param(path: str, param: str, value):
    with open(path) as file:
        config = yaml.safe_load(file)
    for item, doc in config.items():
        if item == param:
            config[param] = value
    with open(path, "w") as file:
        yaml.dump(config, file)