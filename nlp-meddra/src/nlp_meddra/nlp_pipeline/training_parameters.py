import torch
from torch import nn
from project_init import device

def training_init(model, label_weight):
    print(f'training_init device: {device}')
    weights = torch.FloatTensor(label_weight).to(device)
    loss_fn = nn.CrossEntropyLoss(weight=weights).to(device)
    optim = torch.optim.RMSprop(model.parameters(), lr=3e-5)
    return  loss_fn, optim