from dataset import MeddraDataset
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from data_init import remove_special_characters
from dataset import MeddraDataset
import torch
from tqdm import tqdm
from project_init import device
from sklearn.metrics import f1_score, precision_score, recall_score


def generate_array(df_test, no_fever= True):
    df_test_final = df_test[["ZB_LLT_Code","Original_Term", "pt", "ZB_SOC_Code" ]].copy()

    df_test_final['Original_Term'] =df_test_final['Original_Term'].apply(lambda x: str.lower(x))

    df_test_final['Original_Term'] = df_test_final['Original_Term'].apply(lambda x: remove_special_characters(x))
    df_test_final.dropna(inplace =True)
    df_updated = df_test_final[df_test_final.ZB_SOC_Code != 25]
    if no_fever:
        df_updated = df_updated[df_test_final.Original_Term != 'fever']
    df_array_test_new = df_updated.to_numpy()
    return df_array_test_new



def eval_loop(soc_model, pt_model , llt_model, test_dataloader, len_array):
    print(f'in model eval device is {device}')
#     llt_model = llt_model.to(device)
#     pt_model = pt_model.to(device)
#     soc_model = soc_model.to(device)
    soc_model.eval()
    pt_model.eval()
    llt_model.eval()
    correct_predictions_pt = 0
    correct_predictions_soc = 0
    correct_predictions_llt = 0
    count_soc, count_pt, count_llt = 0,0,0
    soc_predicted = []
    soc_actual = []
    pt_predicted = []
    pt_actual = []
    llt_predicted = []
    llt_actual = []
    incorrect_values_pt = {}
    incorrect_values_soc = {}
    incorrect_values_llt = {}
    with torch.no_grad():
        for epoch in tqdm(range(1)):
            for batch in (test_dataloader):
                text = batch['text']
                input_ids = batch["input_ids"]
                attention_mask =batch["attention_mask"]
                token_type_ids = batch["token_type_ids"]
                input_ids = torch.squeeze(input_ids,(1))
                attention_mask = torch.squeeze(attention_mask,(1))
                token_type_ids = torch.squeeze(token_type_ids,(1))
                input_ids = input_ids.to(device)
                attention_mask = attention_mask.to(device)
                token_type_ids = token_type_ids.to(device)
                soc_labels = batch['soc_label'].to(device)
                pt_labels = batch['pt_label'].to(device)
                llt_labels = batch['llt_label'].to(device)
                soc_model_output, soc_outputs, pooled_output_soc= soc_model(input_ids, attention_mask=attention_mask,token_type_ids=token_type_ids)
                pt_model_output, pt_outputs, pooled_output_pt= pt_model(input_ids, attention_mask=attention_mask,token_type_ids=token_type_ids, soc_logits =pooled_output_soc)
                llt_model_output, llt_outputs = llt_model(input_ids, attention_mask=attention_mask,token_type_ids=token_type_ids, soc_logits =pooled_output_soc, pt_logits = pooled_output_pt)
                _, pred_soc = torch.max(soc_outputs, dim=1)
                _, pred_pt = torch.max(pt_outputs, dim=1)
                _, pred_llt = torch.max(llt_outputs, dim=1)
                incorrect_mask_soc = pred_soc != soc_labels
                incorrect_indices_soc= torch.nonzero(incorrect_mask_soc, as_tuple=True)[0]
                incorrect_mask_pt = pred_pt != pt_labels
                incorrect_indices_pt = torch.nonzero(incorrect_mask_pt, as_tuple=True)[0]
                incorrect_mask_llt = pred_llt != llt_labels
                incorrect_indices_llt = torch.nonzero(incorrect_mask_llt, as_tuple=True)[0]
                soc_predicted.extend(pred_soc.cpu().detach().numpy())
                soc_actual.extend(soc_labels.cpu().detach().numpy())
                pt_predicted.extend(pred_pt.cpu().detach().numpy())
                pt_actual.extend(pt_labels.cpu().detach().numpy())
                llt_predicted.extend(pred_llt.cpu().detach().numpy())
                llt_actual.extend(llt_labels.cpu().detach().numpy())
                for i in range(len(incorrect_indices_soc)):
                    idx = incorrect_indices_soc[i] 
                    incorrect_values_soc[count_soc] = {'text' : text[idx], 'soc_label_predicted': pred_soc[idx].item(),'soc_label_actual' : soc_labels[idx].item(),
                                                       
                                            'pt_label_predicted': pred_pt[idx].item(), 'pt_label_actual' : pt_labels[idx].item(),
                                           'llt_label_predicted': pred_llt[idx].item(), 'llt_label_actual' : llt_labels[idx].item() }
                    count_soc += 1
                for i in range(len(incorrect_indices_pt)):
                    idx = incorrect_indices_pt[i] 
                    incorrect_values_pt[count_pt] = {'text' : text[idx], 'soc_label_predicted': pred_soc[idx].item(),'soc_label_actual' : soc_labels[idx].item(),
                                                        
                                                'pt_label_predicted': pred_pt[idx].item(), 'pt_label_actual' : pt_labels[idx].item(),
                                            'llt_label_predicted': pred_llt[idx].item(), 'llt_label_actual' : llt_labels[idx].item() }
                    count_pt +=1
                for i in range(len(incorrect_indices_llt)):
                    idx = incorrect_indices_llt[i] 
                    incorrect_values_llt[count_llt] = {'text' : text[idx], 'soc_label_predicted': pred_soc[idx].item(),'soc_label_actual' : soc_labels[idx].item(),
                                                        
                                                'pt_label_predicted': pred_pt[idx].item(), 'pt_label_actual' : pt_labels[idx].item(),
                                            'llt_label_predicted': pred_llt[idx].item(), 'llt_label_actual' : llt_labels[idx].item() }
                    count_llt +=1
                correct_predictions_soc += torch.sum(pred_soc ==soc_labels)
                correct_predictions_pt += torch.sum(pred_pt ==pt_labels)
                correct_predictions_llt += torch.sum(pred_llt ==llt_labels)
    print(f'count us {count_soc}, {count_pt}, {count_llt}')          
    print(f'length of the array given is: {len_array}')            
    print(f'correctly predicted SOC LABELS: {correct_predictions_soc}')
    print(f'correctly predicted PT LABELS: {correct_predictions_pt}')
    print(f'correctly predicted LLT LABELS: {correct_predictions_llt}')
    print(f'SOC ACCURACY: {correct_predictions_soc/len_array}')
    print(f'PT ACCURACY: {correct_predictions_pt/len_array}')
    print(f'LLT ACCURACY: {correct_predictions_llt/len_array}')
    soc_f1_micro = f1_score(soc_actual, soc_predicted, average='micro')
    soc_precision_micro = precision_score(soc_actual, soc_predicted, average='micro')
    soc_recall_micro = recall_score(soc_actual, soc_predicted, average='micro')
    
    pt_f1_micro = f1_score(pt_actual, pt_predicted, average='micro')
    pt_precision_micro = precision_score(pt_actual, pt_predicted, average='micro')
    pt_recall_micro = recall_score(pt_actual, pt_predicted, average='micro')
    
    llt_f1_micro = f1_score(llt_actual, llt_predicted, average='micro')
    llt_precision_micro = precision_score(llt_actual, llt_predicted, average='micro')
    llt_recall_micro = recall_score(llt_actual, llt_predicted, average='micro')

    print(f'SOC Micro F1 Score: {soc_f1_micro}, Precision: {soc_precision_micro}, Recall: {soc_recall_micro}')
    print(f'PT Micro F1 Score: {pt_f1_micro}, Precision: {pt_precision_micro}, Recall: {pt_recall_micro}')
    print(f'LLT Micro F1 Score: {llt_f1_micro}, Precision: {llt_precision_micro}, Recall: {llt_recall_micro}')
    soc_f1_macro = f1_score(soc_actual, soc_predicted, average='macro')
    soc_precision_macro = precision_score(soc_actual, soc_predicted, average='macro')
    soc_recall_macro = recall_score(soc_actual, soc_predicted, average='macro')
    
    pt_f1_macro = f1_score(pt_actual, pt_predicted, average='macro')
    pt_precision_macro = precision_score(pt_actual, pt_predicted, average='macro')
    pt_recall_macro = recall_score(pt_actual, pt_predicted, average='macro')
    
    llt_f1_macro = f1_score(llt_actual, llt_predicted, average='macro')
    llt_precision_macro = precision_score(llt_actual, llt_predicted, average='macro')
    llt_recall_macro = recall_score(llt_actual, llt_predicted, average='macro')

    print(f'SOC Macro F1 Score: {soc_f1_macro}, Precision: {soc_precision_macro}, Recall: {soc_recall_macro}')
    print(f'PT Macro F1 Score: {pt_f1_macro}, Precision: {pt_precision_macro}, Recall: {pt_recall_macro}')
    print(f'LLT Macro F1 Score: {llt_f1_macro}, Precision: {llt_precision_macro}, Recall: {llt_recall_macro}')
    f1_scores_per_class = f1_score(soc_actual, soc_predicted, average=None)
    precision_scores_per_class = precision_score(soc_actual, soc_predicted, average=None)
    recall_scores_per_class = recall_score(soc_actual, soc_predicted, average=None)
    print(f'SOC Macro F1 Score per class: {f1_scores_per_class}, Precision: {precision_scores_per_class}, Recall: {recall_scores_per_class}')

    # return soc_predicted, pt_predicted, llt_predicted, soc_actual, pt_actual, llt_actual

