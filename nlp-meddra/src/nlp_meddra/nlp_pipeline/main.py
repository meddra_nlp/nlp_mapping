from dataset import MeddraDataset, num_llt_labels, num_pt_labels, num_soc_labels, df_test_array, df_train_array, soc_label_weight, pt_label_weight, llt_label_weight
from project_init import model, tokenizer
from torch import nn
from models import SOCClassifier, PTClassifier, LLTClassifier
from model_train import soc_train, pt_train, llt_train
from model_eval import generate_array, eval_loop
from torch.utils.data import DataLoader
import pandas as pd
import time
import copy
import torch
start = time.time()
print('training...biolink large with additional evaluation')
soc_model = nn.DataParallel(SOCClassifier(model=copy.deepcopy(model),num_soc_labels=num_soc_labels))
pt_model = nn.DataParallel(PTClassifier(model=copy.deepcopy(model),num_pt_labels=num_pt_labels, num_soc_labels=num_soc_labels))
llt_model =nn.DataParallel(LLTClassifier(model=copy.deepcopy(model),num_pt_labels=num_pt_labels, num_soc_labels=num_soc_labels, num_llt_labels=num_llt_labels))

data_train = MeddraDataset(df_train_array, tokenizer=tokenizer)
data_loader_train = DataLoader(data_train, batch_size=512, num_workers=0, shuffle=True)
print(f'length of the training data is {df_train_array.shape}')


# #training 
soc_trained = soc_train(soc_model, 5, data_loader_train, soc_label_weight)
train_soc = time.time()
print(f'SOC Training Done in {train_soc - start}')

pt_trained = pt_train(pt_model, soc_trained, 15, data_loader_train, pt_label_weight)
train_pt = time.time()
print(f'PT Training Done in {train_pt - train_soc}')


llt_trained = llt_train(llt_model, pt_trained, soc_trained, 50, data_loader_train, llt_label_weight)

train_end = time.time()
print(f'LLT Training Done in {train_end - train_pt}')



#evaluation
df_test_v1 = pd.read_csv('data/final_test_data_v1.csv')
df_test_v2 = pd.read_csv('data/final_test_data_v2.csv')

df_test_array_v1 = generate_array(df_test_v1)

df_test_array_v2 = generate_array(df_test_v2)
 
len_array_v1 = len(df_test_array_v1)
len_array_v2 = len(df_test_array_v2)

df_test_loader_v1 = MeddraDataset(df_test_array_v1, tokenizer=tokenizer)
df_test_loader_v2 = MeddraDataset(df_test_array_v2, tokenizer=tokenizer)

data_loader_test_v1= DataLoader(df_test_loader_v1, batch_size=1, num_workers=0, shuffle=False)
data_loader_test_v2= DataLoader(df_test_loader_v2, batch_size=1, num_workers=0, shuffle=False)

print(f'for v1 data: {eval_loop(soc_trained, pt_trained,llt_trained, data_loader_test_v1, len_array_v1)}')
print(f'for v2 data: {eval_loop(soc_trained, pt_trained,llt_trained, data_loader_test_v2, len_array_v2)}')

endtime = time.time()

print(f'Time took for execution: {endtime -start}')
print('IN THIS TRAINING,  distillbert MODEL POOLED STATE is used and abbrevations is true max length is 30, epochs - 5, 15,50 batch size 512')

#save model 

# path = f"sapbert.pt"
# torch.save({
#     'model_soc_state_dict': soc_trained.state_dict(),
#     'model_pt_state_dict': pt_trained.state_dict(),
#     'model_llt_state_dict' :llt_trained.state_dict()
#     }, path)



