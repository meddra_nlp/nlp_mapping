import torch
import torch.nn as nn
from tqdm import tqdm
from training_parameters import training_init



class baseModel(nn.Module):
    def __init__(self, model, num_labels):
        super(baseModel, self).__init__()
        self.model = model
        self.num_labels = num_labels
        self.drop = nn.Dropout(0.3)
        self.linear = nn.Linear(self.model.config.hidden_size, num_labels)
        self.layer_norm = nn.LayerNorm(self.model.config.hidden_size)
        self.softmax = nn.LogSoftmax(dim=1)
    
    def forward(self,input_ids, attention_mask, token_type_ids):
        model =self.model(input_ids=input_ids, attention_mask=attention_mask,token_type_ids=token_type_ids)
        output = self.drop(model["pooler_output"])
        output = self.layer_norm(output)
        output_logits = self.out(output)
        output_softmax = self.softmax(output_logits)
        return model , output_softmax, output_logits
    

class SOCClassifier(baseModel):
    def __init__(self, model, num_soc_labels):
        super(SOCClassifier, self).__init__(model, num_soc_labels)


class PTClassifier(baseModel):
    def __init__(self, model, num_pt_labels, num_soc_labels):
        super(PTClassifier, self).__init__(model, num_pt_labels)
        self.hidden_layer = nn.Linear(self.model.config.hidden_size + num_soc_labels, self.model.config.hidden_size*2)
        self.hidden_layer_2 = nn.Linear(self.model.config.hidden_size*2, self.model.config.hidden_size*4)
        self.layer_norm_1 = nn.LayerNorm(self.model.config.hidden_size*2)
        self.layer_norm_2 = nn.LayerNorm(self.model.config.hidden_size*4)
        self.linear = nn.Linear(self.model.config.hidden_size*4, num_pt_labels)

    def forward(self, input_ids, attention_mask, token_type_ids, soc_logits):
        _, _, output_logits = super().forward(input_ids, attention_mask, token_type_ids)
        output = torch.cat([output_logits, soc_logits], dim = -1)
        output = self.hidden_layer(output)
        output = self.layer_norm_1(output)
        output = torch.relu(output)
        output = self.hidden_layer_2(output)
        output = self.layer_norm_2(output)
        output = torch.relu(output)
        output_logits = self.linear(output)
        output_softmax = self.softmax(output_logits)
        return output_softmax, output_logits
    

class LLTClassifier(baseModel):
    def __init__(self, model, num_llt_labels, num_soc_labels, num_pt_labels):
        super(LLTClassifier, self).__init__(model, num_llt_labels)
        self.hidden_layer = nn.Linear(self.model.config.hidden_size + num_soc_labels + num_pt_labels, self.model.config.hidden_size*2)
        self.hidden_layer_2 = nn.Linear(self.model.config.hidden_size*2, self.model.config.hidden_size*4)
        self.layer_norm_1 = nn.LayerNorm(self.model.config.hidden_size*2)
        self.layer_norm_2 = nn.LayerNorm(self.model.config.hidden_size*4)
        self.linear = nn.Linear(self.model.config.hidden_size*4, num_llt_labels)

    def forward(self, input_ids, attention_mask, token_type_ids, soc_logits, pt_logits):
        _, _, output_logits = super().forward(input_ids, attention_mask, token_type_ids)
        output = torch.cat([output_logits, soc_logits, pt_logits], dim=-1)
        output = self.hidden_layer(output)
        output = self.layer_norm_1(output)
        output = torch.relu(output)
        output = self.hidden_layer_2(output)
        output = self.layer_norm_2(output)
        output = torch.relu(output)
        output_logits = self.linear(output)
        output_softmax = self.softmax(output_logits)
        return output_softmax
    


def train_models(models, epochs, data_loader_train, label_weights, eval_models =None):
    '''
    This function tries to have a generic training workflow of any set given set of models
    Additional Args
    models : A list of models to be trained with the order they appear in the list, here [SOC, PT , LLT]
    label_weight: Also a list of label_weights depending on each models, here[SOC_label_weights, PT_label_weights, LLT_label_weights]
    eval_models: Also a list, here to keep track of models which are not to be trained but only evaluated


    Returns:
    models: list of trained models in the same order as input.
    '''
    assert len(models)> 0 

    if eval is None:
        eval = [None]*len(models)

    trained_models = []

    for i in range(len(models)):
        model = models[i]
        eval_model = eval_models[i]

        loss_fn, optim = training_init(model, label_weight=label_weights[i])


        if eval_model:
            eval_model.eval()

        model.train()
        correct_predictions = 0
        for epoch in tqdm(range(epochs)):
            for batch in data_loader_train:
                optim.zero_grad()
                input_ids = batch["input_ids"]
                attention_mask = batch["attention_mask"]
                token_type_ids = batch["token_type_ids"]
                labels = batch[f'label_{i+1}'].to(device)

                logits = None
                if i == 0:
                    _, outputs, _ = model(input_ids, attention_mask=attention_mask, token_type_ids=token_type_ids)
                    logits = outputs
                else:
                    eval_model_output, eval_model_outputs, _ = eval_model(input_ids, attention_mask=attention_mask, token_type_ids=token_type_ids)
                    _, outputs, _ = model(input_ids, attention_mask=attention_mask, token_type_ids=token_type_ids, logits=eval_model_outputs)
                    logits = outputs

                _, preds = torch.max(logits, dim=1)
                correct_predictions += torch.sum(preds == labels)
                loss = loss_fn(logits, labels)

                loss.backward()
                optim.step()

            print(f'Model {i+1} LOSS: {loss} for the epoch: {epoch}')
        print(f'predicted {correct_predictions} labels correctly in total for Model {i+1}')

        trained_models.append(model)
        
    return trained_models