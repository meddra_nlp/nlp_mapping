"""Project pipelines."""
import os
import sys

#sys.path.append(r"pipelines")

from typing import Dict

from kedro.framework.project import find_pipelines
from kedro.pipeline import Pipeline
from kedro.pipeline.modular_pipeline import pipeline
from nlp_meddra.pipelines import preprocessing as pre
from nlp_meddra.pipelines import stringmatching as sm
from nlp_meddra.pipelines import database as db
from nlp_meddra.pipelines import postprocessing as post
from nlp_meddra.pipelines import experimental as ex
from nlp_meddra.pipelines import fuzzy_matching as fuz


def register_pipelines() -> Dict[str, Pipeline]:
    """Register the project's pipelines.

    Returns:
        A mapping from pipeline names to ``Pipeline`` objects.
    """

    pipeline_database = db.create_pipeline()
    pipeline_preprocessing = pre.create_pipeline()
    pipeline_stringmatching = sm.create_pipeline()
    pipeline_experimental = ex.create_pipeline()
    pipeline_postprocessing = post.create_pipeline()
    pipeline_fuzzy = fuz.create_pipeline()

    database = pipeline(pipe=pipeline_database, outputs={"llts": "llts", "llts_socs": "llts_socs", "llts_with_own_dict": "llts_with_own_dict"})
    preprocessing = pipeline(pipe=pipeline_preprocessing, inputs={"csv_input_preprocessing": "input_data"}, outputs={"csv_add": "csv_output_preprocessing"})
    stringmatching = pipeline(pipe=pipeline_stringmatching, inputs={"csv_input_stringmatching": "csv_output_preprocessing"}, outputs={"csv_spellcheck": "csv_output_stringmatching"})
    experimental = pipeline(pipe=pipeline_experimental, inputs={"csv_input_experimental": "csv_output_stringmatching"}, outputs={"csv_fuzzywuzzy": "csv_output_experimental"})
    postprocessing = pipeline(pipe=pipeline_postprocessing, inputs={"csv_input_postprocessing": "csv_output_stringmatching"})
    postprocessing_experimental = pipeline(pipe=pipeline_postprocessing, inputs={"csv_input_postprocessing": "csv_output_experimental"})
    fuzzy = pipeline(pipe=pipeline_fuzzy, inputs={"csv_input_stringmatching": "csv_output_preprocessing"}, outputs={"csv_fuzzy_match":"csv_output_stringmatching"})

    return {
        "fuzzy": database + preprocessing + fuzzy + postprocessing,
        "experimental": database + preprocessing + stringmatching + experimental + postprocessing_experimental,
        "__default__": database + preprocessing + stringmatching + postprocessing
    }