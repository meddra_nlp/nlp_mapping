import unittest

from app import app, db, nav, bootstrap, moment, ALLOWED_EXTENSIONS

class AppTestCase(unittest.TestCase):

    def setUp(self):
        app.config.update(
            TESTING=True
        )
        self.client = app.test_client()
        self.runner = app.test_cli_runner()

    def tearDown(self):
        pass

    def test_app_exist(self):
        self.assertIsNotNone(app)

    def test_app_is_testing(self):
        self.assertTrue(app.config['TESTING'])

    def test_navigation_bar_exist(self):
        self.assertIsNotNone(nav)

    def test_nav_items_created(self):
        self.assertIn('top', nav.bars)

    def test_database_exist(self):
        self.assertIsNotNone(db)
    
    def test_bootstrap_exist(self):
        self.assertIsNotNone(bootstrap)
    
    def test_moment_exist(self):
        self.assertIsNotNone(moment)
    
    def test_fileform_is_csv(self):
        self.assertIn('csv', ALLOWED_EXTENSIONS)

    