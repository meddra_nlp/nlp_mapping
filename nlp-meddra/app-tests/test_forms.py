import unittest
import io

from app import app

class FormsTestCase(unittest.TestCase):

    def setUp(self):
        app.config.update(
            TESTING=True,
            WTF_CSRF_ENABLED=False
        )
        self.client = app.test_client()

    def tearDown(self):
        pass

    def test_send_message_form_validation(self):
        response = self.client.post('/message_board', data=dict(
            nickname='',
            email='test@nlp.de',
            body='Unit test message.'
        ), follow_redirects=True)
        data = response.get_data(as_text=True)
        # print(data)
        self.assertIn('This field is required', data)

    def test_send_message(self):
        response = self.client.post('/message_board', data=dict(
            nickname='test',
            email='test@nlp.de',
            body='Unit test message.'
        ), follow_redirects=True)
        data = response.get_data(as_text=True)
        self.assertIn('Unit test message.', data)

    def test_single_item_form_validation(self):
        response = self.client.post('/single_item', data=dict(
            studie='Delta',
            ae_term='',
            spec='pneumonia'
        ), follow_redirects=True)
        data = response.get_data(as_text=True)
        self.assertIn('This field is required.', data)

    def test_submit_single_item(self):
        response = self.client.post('/single_item', data=dict(
            studie='',
            ae_term='fever',
            spec=''
        ), follow_redirects=True)
        data = response.get_data(as_text=True)
        # print(data)
        self.assertIn('Choose Method', data)

    def test_file_upload_type_validation(self):
        response = self.client.post('/upload', content_type='multipart/form-data',data=dict(
            file=(io.BytesIO(b"test case"), 'testcase.txt')
        ), follow_redirects=True)
        data = response.get_data(as_text=True)
        # print(data)
        self.assertIn('File type not supported', data)

    def test_no_file_upload(self):
        response = self.client.post('/upload', content_type='multipart/form-data', follow_redirects=True)
        data = response.get_data(as_text=True)
        # print(data)
        self.assertIn('No file part', data)

    def test_no_selected_file(self):
        response = self.client.post('/upload', content_type='multipart/form-data',data=dict(
            file=(io.BytesIO(b"test case"), '')
        ), follow_redirects=True)
        data = response.get_data(as_text=True)
        # print(data)
        self.assertIn('No selected file', data)

    def test_allowed_file_form(self):
        response = self.client.post('/upload', content_type='multipart/form-data',data=dict(
            file=(io.BytesIO(b"test case"), 'testcase.csv')
        ), follow_redirects=True)
        data = response.get_data(as_text=True)
        # print(data)
        self.assertIn('File form not available', data)

    def test__file_upload(self):
        response = self.client.post('/upload', content_type='multipart/form-data',data=dict(
            file=(io.BytesIO(b"AE_ID;Original_Term;\n1;test case"), 'testcase.csv')
        ), follow_redirects=True)
        data = response.get_data(as_text=True)
        # print(data)
        self.assertIn('Choose Method', data)

    def test_choose_method(self):
        response = self.client.post('/choose_method', data='Normal Method', follow_redirects=True)
        data = response.get_data(as_text=True)
        self.assertIn('File Download', data)