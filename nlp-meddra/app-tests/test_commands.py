import unittest

from app import app, db
from app.models import Message
from app.commands import initdb, forge

# Attention !!!
# Following commands may reset all data what are saved in Database
# In official case it is not allowed to lauch
class CommandsTestCase(unittest.TestCase):

    def setUp(self):
        app.config.update(
            TESTING=True,
            SQLALCHEMY_DATABASE_URI='sqlite:////'
        )
        self.client = app.test_client()
        self.runner = app.test_cli_runner()
        with app.app_context():
            db.create_all()

    def tearDown(self):
        with app.app_context():
            db.session.remove()
            db.drop_all()
            self.runner.invoke(forge, ['--count', '139'])

    def test_forge_command(self):
        with app.app_context():
            result = self.runner.invoke(forge)
            self.assertIn('Created 20 fake messages.', result.output)
            self.assertEqual(Message.query.count(), 20)

    def test_forge_command_with_count(self):
        with app.app_context():
            result = self.runner.invoke(forge, ['--count', '50'])
            self.assertIn('Created 50 fake messages.', result.output)
            self.assertEqual(Message.query.count(), 50)

    def test_initdb_command(self):
        result = self.runner.invoke(initdb)
        self.assertIn('Initialized database.', result.output)

    def test_initdb_command_with_drop(self):
        result = self.runner.invoke(initdb, ['--drop'], input='y\n')
        self.assertIn('This operation will delete the database, do you want to continue?', result.output)
        self.assertIn('Drop tables.', result.output)