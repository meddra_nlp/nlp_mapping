import unittest

from app import app

class ViewsTestCase(unittest.TestCase):

    def setUp(self):
        app.config.update(
            TESTING=True,
            WTF_CSRF_ENABLED=False
        )
        self.client = app.test_client()

    def tearDown(self):
        pass

    def test_home_page(self):
        response = self.client.get('/')
        data = response.get_data(as_text=True)
        self.assertIn('Welcome', data)
    
    def test_single_upload_page(self):
        response = self.client.get('/single_item')
        data = response.get_data(as_text=True)
        self.assertIn('Single Input', data)

    def test_upload_page(self):
        response = self.client.get('/upload')
        data = response.get_data(as_text=True)
        self.assertIn('File Upload', data)

    def test_choose_method_page(self):
        response = self.client.get('/choose_method')
        data = response.get_data(as_text=True)
        self.assertIn('Choose Method', data)

    def test_download_page(self):
        response = self.client.get('/download_page')
        data = response.get_data(as_text=True)
        self.assertIn('File Download', data)

    def test_message_board_page(self):
        response = self.client.get('/message_board')
        data = response.get_data(as_text=True)
        self.assertIn('Leave Your Message', data)