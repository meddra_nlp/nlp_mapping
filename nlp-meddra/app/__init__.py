from flask import Flask
from flask_navigation import Navigation
from flask_bootstrap import Bootstrap4
from flask_moment import Moment
from flask_sqlalchemy import SQLAlchemy


ALLOWED_EXTENSIONS = {'csv'}

app = Flask(__name__)
app.config.from_pyfile('settings.py')
app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True


nav = Navigation(app)
bootstrap = Bootstrap4(app)
db = SQLAlchemy(app)
moment = Moment(app)

# initializing Navigations
# extensible
nav.Bar('top', [
    nav.Item('Home', 'index'),
    nav.Item('Single Term', 'single_item'),
    nav.Item('Upload', 'upload_file'),
    nav.Item('Message Board', 'message_board'),
    nav.Item('About Us', 'about_us')
])

from app import views,commands, errors