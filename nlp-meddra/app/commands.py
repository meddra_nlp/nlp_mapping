import click

from app import app, db
from app.models import Message


@app.cli.command()
@click.option('--drop', is_flag=True, help='Create after drop.')
def initdb(drop):
    """Initialize the database."""
    if drop:
        click.confirm('This operation will delete the database, do you want to continue?', abort=True)
        db.drop_all()
        click.echo('Drop tables.')
    db.create_all()
    click.echo('Initialized database.')


@app.cli.command()
@click.option('--count', default=20, help='Quantity of messages, default is 20.')
def forge(count):
    """Generate fake messages."""
    from faker import Faker

    db.drop_all()
    db.create_all()

    fake = Faker()
    click.echo('Working...')

    timestamps = []
    for i in range(count):
        timestamp = fake.date_time_this_year()
        timestamps.append(timestamp)

    timestamps.sort()

    for i in range(count):
        message = Message(
            nickname=fake.name(),
            email=fake.email(),
            body=fake.sentence(),
            timestamp = timestamps[i]
        )
        db.session.add(message)

    db.session.commit()
    click.echo('Created %d fake messages.' % count)