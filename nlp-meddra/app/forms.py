from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, EmailField
from wtforms.validators import DataRequired, Length, Email


class MessageForm(FlaskForm):
    nickname = StringField('Nickname', validators=[DataRequired(), Length(1, 40)])
    email = EmailField('Email', validators=[DataRequired(), Email(), Length(1, 80)])
    body = TextAreaField('Message', validators=[DataRequired(), Length(1, 200)])
    submit = SubmitField()

class ItemForm(FlaskForm):
    ae_term = StringField('AE_Term *', validators=[DataRequired(), Length(1, 40)])
    spec = TextAreaField('AE_Term_Spec(Optional)', validators=[Length(0, 200)])
    submit = SubmitField()