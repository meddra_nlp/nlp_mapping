from flask import render_template, request, flash, redirect, url_for, send_from_directory

from app import app, db, ALLOWED_EXTENSIONS
from app.forms import MessageForm, ItemForm
from app.models import Message

import pandas as pd
import os
import csv


# homepage
@app.route("/")
def index():
    return render_template("index.html")

@app.route("/single_item", methods=['GET', 'POST'])
def single_item():
    form = ItemForm()
    if form.validate_on_submit():
        ae_term = form.ae_term.data
        spec = form.spec.data
        
        file = os.path.join(app.config['UPLOAD_FOLDER'], 'input.csv')
        items = pd.read_csv(file, sep=';', encoding="unicode_escape")
        ae_id = items['AE_ID'].iloc[-1] + 1 if len(items) > 0 else 1
        with open(file, 'w', encoding="utf-8", newline='') as f:
            writer = csv.writer(f, delimiter=";")    
            writer.writerow(["AE_ID", "Original_AE_Term", "Original_AE_Term_Spec"])
            writer.writerow([ae_id, ae_term, spec])


        return redirect(url_for('choose_method'))
    return render_template("single.html", form=form)

# collection of file extensions
# allowed to be uploaded
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def allowed_form(file):
    csv_file = list(file)
    data = csv_file[0]
    form = [item.lower() for item in data[0].replace('"', '').rsplit(';')]
    print(form)

    return "ae_id" in form and ("original_ae_term" in form \
                                and "original_ae_term_spec" in form \
                                      or "original_term" in form)


@app.route("/upload", methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        # check if the file is allowed
        if not allowed_file(file.filename):
            flash('File type not supported')
            return redirect(request.url)
        # save uploaded file locally
        if file and allowed_file(file.filename):
            filename = "input.csv"
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            with open(os.path.join(app.config['UPLOAD_FOLDER'], filename),
             'r', encoding='unicode_escape') as f:
                temp_file = csv.reader(f)
                # check if the file is regular
                if not allowed_form(temp_file):
                    flash('File form not available')
                    return redirect(request.url)
            return redirect(url_for('choose_method')) # offer a link to select different training model
    return render_template("upload.html")

@app.route("/choose_method", methods=['GET', 'POST'])
def choose_method():
    if request.method == 'POST':
        input_pairs = [*request.form.items()]
        pipeline = input_pairs[0][0] # Get pipeline name registered in pipeline.registry and defined in choose.html
        _run(pipeline)
        return redirect(url_for('download_page'))
    return render_template("choose.html")


@app.route("/download_page")
def download_page():
    SAVED_FILES = os.listdir(app.config['DOWNLOAD_FOLDER'])
    if 'output.csv' in SAVED_FILES:
        with open(os.path.join(app.config['DOWNLOAD_FOLDER'], 'output.csv'),
            'r', encoding='unicode_escape') as f:
            terms = pd.read_csv(f, sep=';')
        return render_template("download.html", entries=SAVED_FILES, tables=[terms.to_html(table_id='table')])
    return render_template("download.html", entries=SAVED_FILES)

@app.route("/download/<name>")
def download_file(name):
    return send_from_directory(app.config['DOWNLOAD_FOLDER'], name)

@app.route("/message_board", methods=['GET', 'POST'], defaults={"page": 1})
@app.route("/message_board/<int:page>", methods=['GET', 'POST'])
def message_board(page):
    form = MessageForm()
    if form.validate_on_submit():
        nickname = form.nickname.data
        email = form.email.data
        body = form.body.data
        message = Message(body=body, nickname=nickname, email=email)
        db.session.add(message)
        db.session.commit()
        return redirect(url_for('message_board'))

    pagination = Message.query.order_by(Message.timestamp.desc()).paginate(page=page, per_page=10, error_out=False)
    messages = pagination.items
    return render_template("comments.html", form=form, messages=messages, pagination=pagination)

@app.route("/about_us")
def about_us():
    return render_template("contact.html")


# run pipelines
def _run(pipeline=None):
    from kedro.framework.session import KedroSession
    from kedro.framework.startup import bootstrap_project
    from pathlib import Path

    metadata = bootstrap_project(Path.cwd())
    with KedroSession.create(metadata.package_name) as session:
        session.run(pipeline)

    return pipeline