import os
import sys

from app import app

# SQLite URI compatible
WIN = sys.platform.startswith('win')
if WIN:
    prefix = 'sqlite:///'
else:
    prefix = 'sqlite:////'


dev_db = prefix + os.path.join(os.path.dirname(app.root_path), 'data.db')
UPLOAD_FOLDER = os.path.join(os.getcwd(), 'data', '01_raw')
DOWNLOAD_FOLDER = os.path.join(os.getcwd(), 'data', '07_model_output')

SECRET_KEY = os.getenv('SECRET_KEY', 'dev messages')
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URI', dev_db)